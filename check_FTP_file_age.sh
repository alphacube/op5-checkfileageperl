#!/bin/bash
#################################################################
#           Log on to FTP and get filelist                      #
#           Check time variation against the oldest file        #
#                                                               #
#       By Gustaf Ankarloo                                      #
#       gustaf.ankarloo@cgi.com                                 #
#                                                               #
#                                                               #
#################################################################

#----------------- Functions
function Printhelp() {
        echo " \
        Usage: $(basename $0) -H host -u username -p password
        Where options are any combination of: ( * is not optional )
         * -H                   Ftp host
         * -u                   Ftp username
         * -p|P                 Ftp password | Encrypted password in base64
           -f                   Ftp folder [Default .]
         * -W                   Warning threshold (min)[Default 5]
         * -C                   Critical threshold (min)[Default 10]
           -M                   Set if target FTP is Microsoft [Default is *nix style]
           -d                   Enable debug output
           -h                   Help
        "
}

function f_GetFTP() {
        ftp -iu ${FTPSITE}<<EOF
        user ${FTPUSER} ${FTPPASS}
        cd ${FTPFOLDER}
        dir
        quit
EOF
}

function f_exit() {
        echo "${1} - File is ${DIFF} Minutes old|AGE=${DIFF}Min;${WARN};${CRIT};; file_count=${NRLINES}"
        exit ${2}
}

function f_analyzeFTP() {
        if [[ "${MICROSOFT}" -eq "0" ]]; then
                echo "{$FTPRESULT}" | sort -k6 -k7 -k8| head -n -2 | awk 'NR == 2 {print $6,$7,$8; exit}'
        else
                echo "${FTPRESULT}" | sort -k1 -k2 | head -n1 | awk '{print $1,$2}'
        fi
}

function f_nrLines() {
        echo "${FTPRESULT}" | tail -n +4 | wc -l
}

function f_convertDate() {
        if [[ "${MICROSOFT}" -eq "0" ]]; then
                date --date="${RESULT1}" +%s
        else
                date --date="$(echo ${RESULT1} | sed 's/-/\//g')" +%s
        fi
}
function f_analyzeDate() {
        [[ -z "${RESULTDATE}" ]] && { echo Ivalid Date: is empty; exit 3; }
}
function f_dateDifference() {
        CURRDATE=$(date +%s)
        SUBDATE=$(expr ${CURRDATE} - ${RESULTDATE})
        expr ${SUBDATE} / 60
}
function f_ftpConnectStatus() {
        for i in "${FTPERROR[@]}"
        do
                [[ "${FTPRESULT}" == *${i}* ]] && { echo "Error connecting to FTP: ${i}"; exit 3; }
        done
}
function f_decryptPassword() {
        [[ "${PASSCRYPT}" -eq "1" ]] && { FTPPASS=$(echo "${FTPPASS}" | base64 -d -i); }
}
#----------------- Get commandline arguments
while getopts ":u:p:P:H:f:W:C:dhM" OPT; do
        case ${OPT} in
                u)      FTPUSER=${OPTARG};;
                p)      FTPPASS=${OPTARG};;
                P)      FTPPASS=${OPTARG}; PASSCRYPT=1;;
                H)      FTPSITE=${OPTARG};;
                f)      FTPFOLDER=${OPTARG};;
                W)      WARN=${OPTARG};;
                C)      CRIT=${OPTARG};;
                M)      MICROSOFT=1;;
                h)      Printhelp
                        exit 3;;
                d)      DEBUG=1;;
                :)      echo "Flag -${OPTARG} requires an argument."
                        exit 3;;
                ?)      echo "Invalid Flag -${OPTARG}"
                        Printhelp
                        exit 3;;
        esac
done

#----------------- Variables
[[ -z ${FTPUSER} ]] && { echo Username is required; exit 3; }           # -u
[[ -z ${FTPPASS} ]] && { echo Password is required; exit 3; }           # -p
[[ -z ${FTPSITE} ]] && { echo Server is required; exit 3; }             # -s
[[ -z ${FTPFOLDER} ]] && FTPFOLDER="."                                  # -f
[[ -z ${WARN} ]] && { echo Warning level is required; exit 3; }         # -w
[[ -z ${CRIT} ]] && { echo Critical level is required; exit 3; }        # -c
[[ -z ${MICROSOFT} ]] && MICROSOFT="0"                                  # -M

OP5_OK_CODE=0
OP5_WARN_CODE=1
OP5_CRIT_CODE=2


#----------------- Add possible FTP error output
FTPERROR[0]="Connection timed out"
FTPERROR[1]="unknown host"
FTPERROR[2]="Login failed"

[[  "${DEBUG}" -eq 1 ]] && echo -e FTPUSER = ${FTPUSER}'\n'FTPPASS = ${FTPPASS}'\n'FTPSITE = ${FTPSITE}'\n'FTPFOLDER = ${FTPFOLDER}'\n'WARN = ${WARN}'\n'CRIT = ${CRIT}'\n'
#----------------- Check if Encryption flag is set and then decrypt password
f_decryptPassword

#----------------- Loggin in to FTP and do a file list
FTPRESULT=$(f_GetFTP 2> /dev/null)

#----------------- Check if FTP connection is unsuccessful
#----------------- If output contains whatever text in FTP_ERROR_OUTPUT then exit with status 3
f_ftpConnectStatus

#----------------- Work some magic
if [ ! -z "${FTPRESULT}" ]; then
        # Find Number of lines in result
        NRLINES=$(f_nrLines)
        if [ "${NRLINES}" -ne 0 ]; then
        # Sort files in descending order oldest firts and select the first post
        RESULT1=$(f_analyzeFTP)
        if [ ! -z "${RESULT1}" ]; then
                        # Convert date to unixtime
            RESULTDATE=$(f_convertDate 2> /dev/null)
                        # Analyze Date conversion, if error exit
                        f_analyzeDate
                        # Get Time difference in Minutes
                        DIFF=$(f_dateDifference)
            # If fileage is grater than "$CRITICAL" minutes its Critical
            if [[ "${DIFF}" -gt "${CRIT}" ]]  ; then
                                        f_exit CRITICAL ${OP5_CRIT_CODE}

            elif [[ "${DIFF}" -gt "${WARN}" ]]  ; then
                                        f_exit WARNING ${OP5_WARN_CODE}
            fi
        fi
        fi
fi

[[ -z ${DIFF} ]] && DIFF=0
[[ -z ${NRLINES} ]] && NRLINES=0

f_exit OK ${OP5_OK_CODE}
