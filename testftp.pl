#!/usr/bin/env perl
use 5.014;
use strict;
use warnings;
use autodie;
use Net::FTP;

my ($host, $user, $pass, $folder) = ( '194.16.119.138', 'drift', 'rumsren', 'ftp/Temp' );

my $ftp = Net::FTP->new($host);
$ftp->login($user, $pass);
$ftp->binary();
$ftp->cwd($folder);

my $t = time();
my $t_loc = localtime($t);
my $t_gmt = gmtime($t);

for my $f ($ftp->ls) {
	my $f_mdtm = $ftp->mdtm($f) or next;
	say "File is: $f";

	my $f_loc = localtime($f_mdtm);
	my $f_gmt = gmtime($f_mdtm);
	my $diff = $t - $f_mdtm;
	my $d_loc = localtime($diff);
	my $d_gmt = gmtime($diff);

	printf "%8s: %24s - %-24s\n", "Type", 'Local', 'remote';
	printf "%8s: %24s - %-24s\n", "Epoch", $t, $f_mdtm;
	printf "%8s: %24s - %-24s\n", "Local", $t_loc, $f_loc;
	printf "%8s: %24s - %-24s\n", "GMT", $t_gmt, $f_gmt;
	printf "%8s: %24s - %-24s\n", "Diff", $d_loc, $d_gmt;
	print "\n\n-------------------------------\n\n";
}

