#!/usr/bin/env perl
use feature 'say';
use strict;
use warnings;
use autodie;
use Data::Dumper;
use Getopt::Long qw(GetOptions);
Getopt::Long::Configure qw(gnu_getopt);
use Pod::Usage;
use Net::FTP;
use MIME::Base64;
use Time::localtime qw(ctime);
use Time::Local qw(timelocal);

#################################################
#	Name: check_FTP_file_age.pl					#
#	Author: Gustaf Ankarloo						#
#	Email: gankarloo@gmail.com					#
#	Description:								#
#		Custom chek for OP5/Nagios, checks		#
#		the timestamp of the oldest file on`	#
#		a FTP server and compares it with		#
#		current time.							#
#################################################
# TODO
#
#

#################################################
#			Declare global variables			#
#################################################
my %files;
my $VERBOSE;
my $DEBUG;

#################################################
#				Main Program					#
#################################################

# Print usage if no arguments are supplied
&helptext if $#ARGV lt 0;
# Options that the user must supply on the commandline
my @mandatoryOptions = qw(host user password);
# Options that will get a default value
my @optionswithDefaults = qw(folder warning critical port mode);
# Default values for the above options, listed in the same order
my @defaultOptions = qw(. 5 10 21 1);

# create hashref and interleave the 2 arrays
my $opt =  {
		map{ ($optionswithDefaults[$_], $defaultOptions[$_]) } (0 .. $#optionswithDefaults)
};

# Get cmd options and assign to hash
GetOptions(
	'help|h'		=> sub { &helptext;},
	'user|u=s'		=> \$opt->{user},
	'password|p=s'	=> \$opt->{password},
	'crypted|c=s'	=> sub {$opt->{crypt} = $_[1]; $opt->{password} = 1;},
	'host|H=s'		=> \$opt->{host},
	'folder|f=s'	=> \$opt->{folder},
	'warn|W=i'		=> \$opt->{warning},
	'crit|C=i'		=> \$opt->{critical},
	'port|P'		=> \$opt->{port},
	'active|a'		=> sub {$opt->{mode} = 0},
	'debug|d'		=> \$DEBUG,
	'verbose|v'		=> \$VERBOSE,
) or pod2usage(1);
# check to see we got all required options
foreach (@mandatoryOptions){
	die "Missing mandatory option >>'$_'<<" unless defined $opt->{$_};
}
		print "\t## Commandline options\n", Dumper($opt), "\n\n"		if $DEBUG;

		# If encrypted password provided decrypt.
if ( $opt->{crypt} ) {
	eval {
		chomp( $opt->{password} = decode_base64($opt->{crypt}));
		print "\t ## The password is: >> $opt->{password} << \n\n"		if $DEBUG;
	};
	if ( $@ ) { die "Error decoding password: ", $@; }
}

#################################################
#			Make FTP Connection					#
#################################################

		print "Connecting to FTP server $opt->{host} ... "		if $VERBOSE;

my $ftp = Net::FTP->new($opt->{host}, Passive => $opt->{mode}, Port => $opt->{port} )
	or die "Can't open host $opt->{host}: '$@'";

		print "CONNECTED.\n"									if $VERBOSE;
		print "Authenticating with user $opt->{user} ... "		if $VERBOSE;

$ftp->login($opt->{user}, $opt->{password})
	or die "Cannot login ", $ftp->message;
		print "Logged in\n"										if $VERBOSE;

# Set Binary
$ftp->binary();

# Change directory
$ftp->cwd($opt->{folder})
	or die "Can't cwd to $opt->{folder}: ", $ftp->message;

																	$ftp->hash(1) if $VERBOSE;

		my @dir = $ftp->dir															if $DEBUG;

		print "\t## FTP: full listing\n", Dumper(\@dir), "\n\n"						if $DEBUG;
		printf "\n%-30s: %s\n", 'Filename', 'Modification time'						if $VERBOSE;

for my $file ($ftp->ls) {

		print "\t## FTP message: 'ls' \n", $ftp->message, "\n\n"					if $DEBUG;

	my $mdtm = $ftp->mdtm($file)
		or next;

	my $raw_mdtm;
	$ftp->_MDTM($file)
		or next;

	if ( $ftp->message =~ /((\d\d)(\d\d\d?))(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/ ) {
		$raw_mdtm = timelocal($8, $7, $6, $5, $4 - 1, $2 eq '19' ? $3 : ($1 - 1900))
	}
		
		printf "%-30s: %d\n", $file, $mdtm											if $VERBOSE;
		printf "%-30s: %d\n", $file, $raw_mdtm											if $VERBOSE;
		my $mdtmstamp = ctime($mdtm);
		my $mdtmgm = gmtime($mdtm);
		my @gm = gmtime($mdtm);
		my $newMdtm = timelocal(@gm);
		my $mdtmlo = localtime($mdtm);
		my $rmdtmstamp = ctime($raw_mdtm);
		my $rmdtmgm = gmtime($raw_mdtm);
		my $rmdtmlo = localtime($raw_mdtm);
		printf "mdtm %-30s: %s\n", $file, $mdtmstamp										if $VERBOSE;
		printf "mdtm %-30s: %s\n", $file, $mdtmgm										if $VERBOSE;
		printf "mdtm %-30s: %s\n", $file, $mdtmlo										if $VERBOSE;
		printf "raw %-30s: %s\n", $file, $rmdtmstamp										if $VERBOSE;
		printf "raw %-30s: %s\n", $file, $rmdtmgm										if $VERBOSE;
		printf "raw %-30s: %s\n", $file, $rmdtmlo										if $VERBOSE;
		printf "new %-30s: %s\n", $file, $newMdtm										if $VERBOSE;
		print "\t## FTP message: 'mdtm' \n", $ftp->message, "\n\n"					if $VERBOSE;
		print "\t## FTP message: 'mdtm' \n", $ftp->message, "\n\n"					if $DEBUG;

	$files{$file} = $newMdtm;
}
		print "\n\n"																if $VERBOSE;
		print "\t## Contents of hash \%files\n", Dumper(\%files), "\n\n"			if $DEBUG;
$ftp->quit;


#################################################
#			End of FTP session					#
#################################################


#################################################
#			Calculate difftime and				#
#			report the findings					#
#################################################
my $currtime = time();
			say "Current Time is = $currtime"												if $VERBOSE;
my $timestamp = ctime($currtime);
			say "Ctime Time is    >> $timestamp <<"										if $VERBOSE;
			my $ltime = gmtime($currtime);
			say "gmtime Time is   ## $ltime ##"							if $VERBOSE;
my $nrfiles = scalar keys %files;
			say "Number of files is = $nrfiles"												if $VERBOSE;
my $oldest = 0;
my $difftime = 0;

if ( keys %files > 0 ) {
	for (keys %files) {
		next if $_ eq 'oldfile';
		my $diff = $currtime - $files{$_};
		if ( $diff < 0 ) {
			$diff = 0;
			say "Modification time is in the future"										if $VERBOSE;
		}
		say "File: '$_' modtime: '$files{$_}' currtime: '$currtime' Diff: ", $diff				if $VERBOSE;
		if ( $diff > $oldest ) {
			$oldest = $diff;
			$files{oldfile} = $_;
		}

	}
	if ( $files{oldfile} ) {
		say "oldfile: $files{oldfile}"														if $VERBOSE;
		say "time: $files{$files{oldfile}}"													if $VERBOSE;
		$difftime = sprintf "%.0f", $oldest/60;
				say "The oldest file is: '$files{oldfile}' "								if $VERBOSE;
				say "and it is $difftime minutes old "										if $VERBOSE;
				say "or in seconds ", $oldest											if $VERBOSE;
				say "Number of files is: ", $nrfiles										if $VERBOSE;
				say ""																		if $VERBOSE;
				say "Crit value is: $opt->{critical}"										if $VERBOSE;
				say "Warn value is: $opt->{warning}"										if $VERBOSE;
				print Dumper(\%files)														if $DEBUG;
	}
	# Check difftime against warn and crit.
	## I difftime is greater or equal than CRITICAL
	if ( $difftime > $opt->{critical} ) {
		say "--CRITITCAL--"																if $VERBOSE;
		&exitcode('CRITICAL', 2, $difftime, $opt->{warning}, $opt->{critical}, $nrfiles);
	}
	## If difftime is greater or equal than WARNING
	if ( $difftime > $opt->{warning} ) {
		say "--WARNING--"																if $VERBOSE;
		&exitcode('WARNING', 1, $difftime, $opt->{warning}, $opt->{critical}, $nrfiles);
	}
}
	## Else all is good
	&exitcode('OK', 0, $difftime, $opt->{warning}, $opt->{critical}, $nrfiles);

	##################################
	##			Subroutines			 #
	##################################
	sub exitcode {
		#	printf "%s - File is %i Minutes old|AGE=%iMin;%i;%i;; file_count=%i\n", $_[0], $difftime, $difftime, $opt->{warning}, $opt->{critical}, $nrfiles;
		printf "%s - File is %i Minutes old|AGE=%iMin;%i;%i;; file_count=%i\n", $_[0], $_[2], $_[2], $_[3], $_[4], $_[5];
		exit($_[1]);
	}

sub helptext {
say "		Usage: check_FTP_file_age.pl -H host -u username -p password";
say "		Where options are any combination of: ( * is mandatory )";
say "		Valid syntax is shorthand [ -h ] or longhand [ --help ]";
say "			-h|help				Print this text.";
say "		*	-H|host				Ftp host";
say "		*	-u|user				Ftp username";
say "		*	-p|password			Ftp password in cleartext";
say "			-c|crypted			Ftp password encrypted with Base64";
say "			-f|folder			Ftp folder [Default .]";
say "		*	-W|warn				Warning threshold (min)";
say "		*	-C|crit				Critical threshold (min)";
say "			-P|port				Ftp port. Default is 21";
say "			-a|active			Ftp Active mode. Default Passive";
say "			-d 				Enable debug output";
say "			-v 				Verbose output";
exit(0);
}
