#!/usr/bin/env perl
use 5.014;
use strict;
use warnings;
use autodie;
use Data::Dumper;
use Getopt::Long qw(GetOptions);
Getopt::Long::Configure qw(gnu_getopt);
use Pod::Usage;
use Net::FTP;
use MIME::Base64;
my %files;
my @mandatoryOptions = qw(host user password);
my @optionswithDefaults = qw(folder warning critical port mode);
my @defaultOptions = qw(. 5 10 21 1);
my $opt =  {
};
GetOptions(
	'help|h'		=> sub { &helptext;},
	'user|u=s'		=> \$opt->{user},
	'password|p=s'	=> \$opt->{password},
	'crypted|c=s'	=> sub {$opt->{crypt} = $_[1]; $opt->{password} = 1;},
	'host|H=s'		=> \$opt->{host},
	'folder|f=s'	=> \$opt->{folder},
	'warn|W=i'		=> \$opt->{warning},
	'crit|C=i'		=> \$opt->{critical},
	'port|P'		=> \$opt->{port},
	'active|a'		=> sub {$opt->{mode} = 0},
) or pod2usage(1);
foreach (@mandatoryOptions){
	die "Missing mandatory option >>'$_'<<" unless defined $opt->{$_};
}
if ( $opt->{crypt} ) {
	eval {
		chomp( $opt->{password} = decode_base64($opt->{crypt}));
	};
	if ( $@ ) { die "Error decoding password: ", $@; }
}
my $ftp = Net::FTP->new($opt->{host}, Passive => $opt->{mode}, Port => $opt->{port} )
	or die "Can't open host $opt->{host}: '$@'";
$ftp->login($opt->{user}, $opt->{password})
	or die "Cannot login ", $ftp->message;
$ftp->binary();
$ftp->cwd($opt->{folder})
	or die "Can't cwd to $opt->{folder}: ", $ftp->message;
my @dir = $ftp->dir;
for my $file ($ftp->ls) {
	my $mdtm = $ftp->mdtm($file)
		or next;
	$files{$file} = $mdtm;
}
$ftp->quit;
my $currtime = time;
my $nrfiles = scalar keys %files;
my $oldest = 0;
for (keys %files) {
	next if $_ eq 'oldfile';
	my $diff = $currtime - $files{$_};
	if ( $diff > $oldest ) {
		$oldest = $diff;
		$files{oldfile} = $_;
	}
}
my $difftime = sprintf "%.0f", $oldest/60;
if ( $difftime > $opt->{critical} ) {
	&exitcode('CRITICAL', 2);
}
if ( $difftime > $opt->{warning} ) {
	&exitcode('WARNING', 1);
}
&exitcode('OK', 0);
sub exitcode {
	printf "%s - File is %i Minutes old|AGE=%iMin;%i;%i;; file_count=%i\n", $_[0], $difftime, $difftime, $opt->{warning}, $opt->{critical}, $nrfiles;
	exit($_[1]);
}
sub helptext {
say "		Usage: check_FTP_file_age.pl -H host -u username -p password";
say "		Where options are any combination of: ( * is mandatory )";
say "		Valid syntax is shorthand [ -h ] or longhand [ --help ]";
say "			-h|help				Print this text.";
say "		*	-H|host				Ftp host";
say "		*	-u|user				Ftp username";
say "		*	-p|password			Ftp password in cleartext";
say "			-c|crypted			Ftp password encrypted with Base64";
say "			-f|folder			Ftp folder [Default .]";
say "		*	-W|warn				Warning threshold (min)";
say "		*	-C|crit				Critical threshold (min)";
say "			-P|port				Ftp port. Default is 21";
say "			-a|active			Ftp Active mode. Default Passive";
say "			-d 				Enable debug output";
say "			-v 				Verbose output";
exit(0);
}
